# Car Insurance API

Service for car resellers to incorporate the insurance when selling a car. 
Provide car resellers with a REST API, so they can integrate the insurance quoting on their
own systems.

## Quickstart

The application is deployed to [Bitbucket](https://bitbucket.org/lmmedeiros/car-insurance-api/branches/?status=merged).

To download the application, you need to clone the Git repository:

```
git clone https://lmmedeiros@bitbucket.org/lmmedeiros/car-insurance-api.git
cd car-insurance-api
git fetch && git checkout master
```

The projects are targeting .NET Framework 4.6.1 and the latest version of C# (currently 7.2).

Once you open the solution in Visual Studio, you need to restore the nuget packages (right click in solution > "Restore NuGet packages") to install the application dependencies.


## Architecture

├── Sample


│   ├── CarInsurance.Sample


├── Tests


│   ├── ...


├── CarInsurance.Entities


├── CarInsurance.Repositories


├── CarInsurance.Services


├── CarInsurance.Web


└── README.md

**CarInsurance.Entities** - Project that contains all the Domain entities and value objects. All projects in the solution contain a dependency to this project.

**CarInsurance.Services** - Project that contain the services that encompass the Car Insurance application. It contains the Domain logic and business directives.

**CarInsurance.Repositories** - 
    Project that communicates with the database to retrieve information. It relies on Entity Framework to map the data.
    The database context is configured to automatically create the MSSQL local database `CarInsurance` whenever the model changes.

**CarInsurance.Web** - Web project that holds the Quoting Web API. The api consists of three resources:

- POST: Creates a new quote (HTTP Post to /api/quoting/).

- GET: Get complete quote (HTTP Get to /api/quoting/{quoteNumber}).

- GET Summary: Get summary with quote status and price (HTTP Get to /api/quoting/{quoteNumber}/summary).


To test the API, you can use this [Postman collection](https://www.getpostman.com/collections/07f69f409282d9722d35). If you're not familiar to how Postman works, check its [documentation](https://www.getpostman.com/).

The web project has a dependency in StructureMap to achieve IoC and map the interfaces to its implementations.

**Tests** - Each solution project also has a related Unit Test project with similar structure. All the behaviors and logic in the project must have test coverage.

**Sample** - Console application to test the integration with the service. It uses the real implementation and not mocks.


## Guidelines

As a general rule, follow these code conventions to add new features to the application:

- Use Visual Studio defaults.

- Classes should have a single responsibility (SRP).

- Code for interfaces. All the strategies should be [nested](https://marketplace.visualstudio.com/items?itemName=MadsKristensen.FileNesting) into the interface file.

- Interfaces are explicitly implemented.


Testing Guidelines:

- Ensure full coverage in your unit tests.

- Use Arrange/Act/Assert.

- Tests should execute independently and not rely on execution order.

- Avoid shared properties. Prefer to use `private` methods or `internal` libraries to init the Arranges.