﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Net.Mail;

namespace CarInsurance.Entities
{
    public class Customer
    {
        public Customer(string socialSecurityNumber, string name, Gender gender, DateTime birthDate, string address,
            string email, string phoneNumber)
        {
            Name = name ??
                throw new ArgumentNullException(nameof(name));

            SocialSecurityNumber = socialSecurityNumber ??
                throw new ArgumentNullException(nameof(socialSecurityNumber));

            Address = address;
            BirthDate = birthDate;
            Email = new MailAddress(email).Address;
            Gender = gender;
            PhoneNumber = phoneNumber;
        }

        private Customer() { }

        [Key]
        public string SocialSecurityNumber { get; private set; }

        public string Address { get; private set; }

        public DateTime BirthDate { get; private set; }

        public string Email { get; private set; }

        public Gender Gender { get; private set; }

        public string Name { get; private set; }

        public string PhoneNumber { get; private set; }
    }
}
