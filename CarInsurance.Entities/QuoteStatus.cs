﻿namespace CarInsurance.Entities
{
    public enum QuoteStatus
    {
        Created,
        Saved,
        Expired
    }
}
