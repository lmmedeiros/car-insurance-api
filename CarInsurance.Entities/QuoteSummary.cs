﻿using System;

namespace CarInsurance.Entities
{
    public class QuoteSummary
    {
        public QuoteSummary(Guid quoteNumber, decimal price, QuoteStatus quoteStatus)
        {
            Price = price;
            QuoteNumber = quoteNumber;
            QuoteStatus = quoteStatus;
        }

        public decimal Price { get; }

        public Guid QuoteNumber { get; }

        public QuoteStatus QuoteStatus { get; }
    }
}
