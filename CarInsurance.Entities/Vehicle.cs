﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CarInsurance.Entities
{
    public class Vehicle
    {
        public Vehicle(VehicleType type, int manufacturingYear, string model, string make)
        {
            Make = make ?? throw new ArgumentNullException(nameof(make));
            ManufacturingYear = manufacturingYear;
            Model = model ?? throw new ArgumentNullException(nameof(model));
            Type = type;
        }

        private Vehicle() { }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; private set; }

        public string Make { get; private set; }

        public int ManufacturingYear { get; private set; }
        
        public string Model { get; private set; }

        public VehicleType Type { get; private set; }
    }
}
