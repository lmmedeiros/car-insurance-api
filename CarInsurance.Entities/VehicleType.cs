﻿namespace CarInsurance.Entities
{
    public enum VehicleType
    {
        Car,
        Motocycle,
        Truck,
        Other
    }
}
