﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CarInsurance.Entities
{
    public class VehicleInsurancePrice
    {
        public VehicleInsurancePrice(int incidentAveragePerYear, decimal basePrice, VehicleType type, 
            int? manufacturingYear, string model, string make)
        {
            BasePrice = basePrice;
            IncidentAveragePerYear = incidentAveragePerYear;
            Make = make;
            ManufacturingYear = manufacturingYear;
            Model = model;
            Type = type;
        }

        private VehicleInsurancePrice() { }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; private set; }

        public decimal BasePrice { get; private set; }

        public int IncidentAveragePerYear { get; private set; }

        public string Make { get; private set; }

        public int? ManufacturingYear { get; private set; }

        public string Model { get; private set; }

        public VehicleType Type { get; private set; }
    }
}
