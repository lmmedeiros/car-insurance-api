﻿using System;
using System.ComponentModel.DataAnnotations;

namespace CarInsurance.Entities
{
    public class Quote
    {
        public Quote(Customer customer, Vehicle vehicle)
        {
            QuoteNumber = Guid.NewGuid();
            QuoteStatus = QuoteStatus.Created;

            Customer = customer ?? throw new ArgumentNullException(nameof(customer));
            Vehicle = vehicle ?? throw new ArgumentNullException(nameof(vehicle));
        }

        private Quote() { }

        [Key]
        public Guid QuoteNumber { get; private set; }

        public Customer Customer { get; private set; }

        public Vehicle Vehicle { get; private set; }

        public QuoteStatus QuoteStatus { get; set; }
    }
}
