﻿using System;
using System.Threading.Tasks;
using System.Web.Http.Results;
using CarInsurance.Entities;
using CarInsurance.Entities.Tests.MockTemplates;
using CarInsurance.Services.Quoting;
using CarInsurance.Web.Controllers.api;
using CarInsurance.Web.Models.Quote;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace CarInsurance.Web.Tests.Controllers.api
{
    [TestClass]
    public class QuotingControllerTests
    {
        [TestMethod]
        public async Task PostingShouldReturnBadRequestWhenModelStateIsInvalid()
        {
            // Arrange
            const string error = "error";

            var controller = new QuotingController(Mock.Of<IQuoteProcessorService>(), 
                Mock.Of<IQuoteRetrieverService>());

            controller.ModelState.AddModelError("error", error);

            var request = new QuotingRequest
            {
                Customer = CustomerTemplate.Customer,
                Vehicle = VehicleTemplate.Vehicle
            };

            // Act
            var response = await controller.Post(request);

            // Assert
            Assert.IsInstanceOfType(response, typeof(InvalidModelStateResult));
        }

        [TestMethod]
        public async Task PostingShouldReturnQuoteNumber()
        {
            // Arrange
            var quoteNumber = Guid.Parse("6ed26ad4-c7e2-437b-9c2b-f06b29391bba");
            var request = new QuotingRequest
            {
                Customer = CustomerTemplate.Customer,
                Vehicle = VehicleTemplate.Vehicle
            };

            var processor = Mock.Of<IQuoteProcessorService>(p =>
                p.ProcessAsync(request.Customer, request.Vehicle) == Task.FromResult(quoteNumber));

            var controller = new QuotingController(processor, Mock.Of<IQuoteRetrieverService>());

            // Act
            var result = await controller.Post(request) as OkNegotiatedContentResult<Guid>;

            // Assert
            Assert.IsNotNull(result);
            Assert.AreEqual(quoteNumber, result.Content);
        }

        [TestMethod]
        public void GettingQuoteShouldReturnNotFoundResultWhenQuoteRetrieverServiceThrowsInvalidOperationException()
        {
            // Arrange
            var quoteNumber = Guid.Empty;
            var retrieverMock = new Mock<IQuoteRetrieverService>();
            retrieverMock.Setup(r => r.Get(quoteNumber)).Throws<InvalidOperationException>();

            var controller = new QuotingController(Mock.Of<IQuoteProcessorService>(), retrieverMock.Object);

            // Act
            var result = controller.GetQuote(quoteNumber);

            // Assert
            Assert.IsInstanceOfType(result, typeof(NotFoundResult));
        }

        [TestMethod]
        public void GettingQuoteShouldReturnRetrieverServiceQuote()
        {
            // Arrange
            var quote = new Quote(CustomerTemplate.Customer, VehicleTemplate.Vehicle);
            var retriever = Mock.Of<IQuoteRetrieverService>(r => r.Get(quote.QuoteNumber) == quote);
            var controller = new QuotingController(Mock.Of<IQuoteProcessorService>(), retriever);

            // Act
            var result = controller.GetQuote(quote.QuoteNumber) as OkNegotiatedContentResult<Quote>;

            // Assert
            Assert.IsNotNull(result);
            Assert.AreSame(quote, result.Content);
        }

        [TestMethod]
        public void GettingSummaryShouldReturnNotFoundResultWhenQuoteRetrieverServiceThrowsInvalidOperationException()
        {
            // Arrange
            var quoteNumber = Guid.Empty;
            var retrieverMock = new Mock<IQuoteRetrieverService>();
            retrieverMock.Setup(r => r.GetSummary(quoteNumber)).Throws<InvalidOperationException>();

            var controller = new QuotingController(Mock.Of<IQuoteProcessorService>(), retrieverMock.Object);

            // Act
            var result = controller.GetSummary(quoteNumber);

            // Assert
            Assert.IsInstanceOfType(result, typeof(NotFoundResult));
        }

        [TestMethod]
        public void GettingSummaryShouldReturnRetrieverServiceQuoteSummary()
        {
            // Arrange
            var quoteNumber = Guid.Parse("6ed26ad4-c7e2-437b-9c2b-f06b29391bba");
            var summary = new QuoteSummary(quoteNumber, 1m, QuoteStatus.Expired);
            var retriever = Mock.Of<IQuoteRetrieverService>(r => r.GetSummary(quoteNumber) == summary);
            var controller = new QuotingController(Mock.Of<IQuoteProcessorService>(), retriever);

            // Act
            var result = controller.GetSummary(quoteNumber) as OkNegotiatedContentResult<QuoteSummary>;

            // Assert
            Assert.IsNotNull(result);
            Assert.AreSame(summary, result.Content);
        }
    }
}
