﻿using CarInsurance.Web.DependencyResolver;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using StructureMap;

namespace CarInsurance.Web.Tests.DependencyResolver
{
    [TestClass]
    public class CommonRegistryTests
    {
        [TestMethod]
        public void ValidatingContainerConfigurationShouldAssertAllDependencies()
        {
            // Arrange
            var container = new Container(new CommonRegistry());

            // Act && Assert
            container.AssertConfigurationIsValid();
        }
    }
}
