﻿using System;
using System.Threading.Tasks;
using CarInsurance.Entities;
using CarInsurance.Repositories;
using CarInsurance.Repositories.DataContext;
using CarInsurance.Services;
using CarInsurance.Services.Quoting;

namespace CarInsurance.Sample
{
    class Program
    {
        static void Main(string[] args)
        {
            MainAsync(args).Wait();
        }

        async static Task MainAsync(string[] args)
        {
            var context = new CarInsuranceContext();

            IQuoteProcessorService processor = new QuoteProcessorService(
                                                new QuoteBuilder(),
                                                new QuoteCreationService(
                                                    new QuoteRepository(
                                                        context,
                                                        new CustomerRepository(context)
                                                    )));

            var customer = new Customer("4", "Lucas", Gender.Male, new DateTime(1993, 9, 16), null, "lucas@email.com", null);

            var vehicle = new Vehicle(VehicleType.Car, 2000, "100", "Audi");

            var quoteNumber = await processor.ProcessAsync(customer, vehicle);

            IQuoteRetrieverService quoteRetrieverService = new CachedQuoteRetrieverServiceDecorator(
                                                                new QuoteRetrieverService(
                                                                    new QuoteFinderRepository(context),
                                                                        new InsurancePriceRetrieverService(
                                                                            new VehicleInsurancePriceRepository(context)
                                                                        )));

            var summary = quoteRetrieverService.GetSummary(quoteNumber);

            Console.WriteLine("Summary:");
            Console.WriteLine(summary.QuoteNumber);
            Console.WriteLine(summary.Price);
            Console.WriteLine(summary.QuoteStatus);
            Console.WriteLine();

            var quote = quoteRetrieverService.Get(quoteNumber);
            quote = quoteRetrieverService.Get(quoteNumber);

            Console.WriteLine("Quote:");
            Console.WriteLine(quote.QuoteNumber);
            Console.WriteLine(quote.Customer.Email);
            Console.WriteLine(quote.Vehicle.Type);
        }
    }
}
