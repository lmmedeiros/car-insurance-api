﻿using CarInsurance.Entities;

namespace CarInsurance.Web.Models.Quote
{
    public class QuotingRequest
    {
        public Customer Customer { get; set; }

        public Vehicle Vehicle { get; set; }
    }
}