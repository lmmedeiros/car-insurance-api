﻿using CarInsurance.Repositories.Common;
using CarInsurance.Repositories.DataContext;
using CarInsurance.Services;
using CarInsurance.Services.Quoting;
using StructureMap;

namespace CarInsurance.Web.DependencyResolver
{
    public class CommonRegistry : Registry
    {
        public CommonRegistry()
        {
            Scan(_ =>
            {
                _.TheCallingAssembly();
                _.AssemblyContainingType<IQuoteBuilder>();
                _.AssemblyContainingType<CarInsuranceContext>();
                _.WithDefaultConventions();
                _.RegisterConcreteTypesAgainstTheFirstInterface();
                _.SingleImplementationsOfInterface();

                _.AddAllTypesOf(typeof(IFactory<,>));
                _.AddAllTypesOf(typeof(IFinder<,>));
                _.AddAllTypesOf(typeof(ISaveAsync<>));
            });

            For<CarInsuranceContext>().Transient();

            For<IQuoteRetrieverService>().Use<QuoteRetrieverService>();
            For<IQuoteRetrieverService>().DecorateAllWith<CachedQuoteRetrieverServiceDecorator>();
        }
    }
}