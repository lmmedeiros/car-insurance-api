﻿using System;
using System.Threading.Tasks;
using System.Web.Http;
using CarInsurance.Services.Quoting;
using CarInsurance.Web.Models.Quote;

namespace CarInsurance.Web.Controllers.api
{
    [RoutePrefix("api/quoting")]
    public class QuotingController : ApiController
    {
        private readonly IQuoteProcessorService _quoteProcessorService;
        private readonly IQuoteRetrieverService _quoteRetrieverService;

        public QuotingController(IQuoteProcessorService quoteProcessorService,
            IQuoteRetrieverService quoteRetrieverService)
        {
            _quoteProcessorService = quoteProcessorService;
            _quoteRetrieverService = quoteRetrieverService;
        }

        [HttpPost]
        [Route("")]
        public async Task<IHttpActionResult> Post(QuotingRequest model)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            return Ok(await _quoteProcessorService.ProcessAsync(model.Customer, model.Vehicle));
        }

        [HttpGet]
        [Route("{quoteNumber}")]
        public IHttpActionResult GetQuote(Guid quoteNumber)
        {
            try
            {
                return Ok(_quoteRetrieverService.Get(quoteNumber));
            } 
            catch (InvalidOperationException)
            {
                return NotFound();
            }
        }

        [HttpGet]
        [Route("{quoteNumber}/summary")]
        public IHttpActionResult GetSummary(Guid quoteNumber)
        {
            try
            {
                return Ok(_quoteRetrieverService.GetSummary(quoteNumber));
            }
            catch (InvalidOperationException)
            {
                return NotFound();
            }
        }
    }
}
