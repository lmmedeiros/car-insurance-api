﻿using System;
using CarInsurance.Entities;
using CarInsurance.Repositories;

namespace CarInsurance.Services
{
    public class InsurancePriceRetrieverService : IInsurancePriceRetrieverService
    {
        private readonly IVehicleInsurancePriceFinder _vehicleInsurancePriceRepository;

        public InsurancePriceRetrieverService(IVehicleInsurancePriceFinder vehicleInsurancePriceRepository)
        {
            _vehicleInsurancePriceRepository = vehicleInsurancePriceRepository;
        }

        decimal IInsurancePriceRetrieverService.GetPrice(Vehicle vehicle)
        {
            if (vehicle == null)
                throw new ArgumentNullException(nameof(vehicle));

            const decimal basePrice = 1000m;

            var insurancePrice =
                _vehicleInsurancePriceRepository.Find(vehicle) ??
                _vehicleInsurancePriceRepository.FindByTypeAndYearAndMake(
                    vehicle.Type, vehicle.ManufacturingYear, vehicle.Make) ??
                _vehicleInsurancePriceRepository.FindByTypeAndYear(vehicle.Type, vehicle.ManufacturingYear) ??
                _vehicleInsurancePriceRepository.FindByType(vehicle.Type);

            return insurancePrice?.BasePrice ?? basePrice;
        }
    }
}
