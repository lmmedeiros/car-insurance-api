﻿using CarInsurance.Entities;

namespace CarInsurance.Services
{
    public interface IInsurancePriceRetrieverService
    {
        decimal GetPrice(Vehicle vehicle);
    }
}
