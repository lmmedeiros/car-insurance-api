﻿using CarInsurance.Entities;

namespace CarInsurance.Services.Quoting
{
    public interface IQuoteBuilder
    {
        Quote Build(Customer customer, Vehicle vehicle);
    }
}
