﻿using System;
using System.Threading.Tasks;
using CarInsurance.Entities;
using CarInsurance.Repositories.Common;

namespace CarInsurance.Services.Quoting
{
    public class QuoteCreationService : IQuoteCreationService
    {
        private readonly ISaveAsync<Quote> _quoteRepository;

        public QuoteCreationService(ISaveAsync<Quote> quoteRepository)
        {
            _quoteRepository = quoteRepository;
        }

        async Task IQuoteCreationService.CreateQuoteAsync(Quote quote)
        {
            if (quote == null)
                throw new ArgumentNullException(nameof(quote));

            await _quoteRepository.SaveAsync(quote);
        }
    }
}
