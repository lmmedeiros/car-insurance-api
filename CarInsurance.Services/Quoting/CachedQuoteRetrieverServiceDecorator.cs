﻿using System;
using System.Linq;
using System.Collections.Generic;
using CarInsurance.Entities;

namespace CarInsurance.Services.Quoting
{
    public sealed class CachedQuoteRetrieverServiceDecorator : QuoteRetrieverServiceDecoratorBase, IQuoteRetrieverService
    {
        private static ICollection<Quote> _cachedQuotes = new List<Quote>();
        private readonly IQuoteRetrieverService _quoteRetrieverService;

        public CachedQuoteRetrieverServiceDecorator(IQuoteRetrieverService quoteRetrieverService)
            : base(quoteRetrieverService)
        {
            _quoteRetrieverService = quoteRetrieverService;
        }

        internal CachedQuoteRetrieverServiceDecorator(IQuoteRetrieverService quoteRetrieverService,
            IEnumerable<Quote> quotesCollection) : this(quoteRetrieverService)
        {
            _cachedQuotes = quotesCollection.ToArray();
        }

        Quote IQuoteRetrieverService.Get(Guid quoteNumber)
        {
            var quote = _cachedQuotes.SingleOrDefault(c => c.QuoteNumber == quoteNumber);

            if (quote == null)
            {
                quote = _quoteRetrieverService.Get(quoteNumber);
                _cachedQuotes.Add(quote);
            }

            return quote;
        }
    }
}
