﻿using System;
using CarInsurance.Entities;
using CarInsurance.Repositories.Common;

namespace CarInsurance.Services.Quoting
{
    public class QuoteRetrieverService : IQuoteRetrieverService
    {
        private readonly IInsurancePriceRetrieverService _insurancePriceRetrieverService;
        private readonly IFinder<Guid, Quote> _quoteFinder;

        public QuoteRetrieverService(IFinder<Guid, Quote> quoteFinder,
            IInsurancePriceRetrieverService insurancePriceRetrieverService)
        {
            _quoteFinder = quoteFinder;
            _insurancePriceRetrieverService = insurancePriceRetrieverService;
        }

        Quote IQuoteRetrieverService.Get(Guid quoteNumber)
        {
            return FindQuote(quoteNumber);
        }

        QuoteSummary IQuoteRetrieverService.GetSummary(Guid quoteNumber)
        {
            var quote = FindQuote(quoteNumber);
            var price = _insurancePriceRetrieverService.GetPrice(quote.Vehicle);

            return new QuoteSummary(quote.QuoteNumber, price, quote.QuoteStatus);
        }

        private Quote FindQuote(Guid quoteNumber)
        {
            var quote = _quoteFinder.Find(quoteNumber);

            if (quote == null)
                throw new InvalidOperationException("This operation requires a valid Quote");

            return quote;
        }
    }
}
