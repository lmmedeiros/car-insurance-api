﻿using System.Threading.Tasks;
using CarInsurance.Entities;

namespace CarInsurance.Services.Quoting
{
    public interface IQuoteCreationService
    {
        Task CreateQuoteAsync(Quote quote);
    }
}
