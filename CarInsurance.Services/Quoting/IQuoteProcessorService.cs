﻿using System;
using System.Threading.Tasks;
using CarInsurance.Entities;

namespace CarInsurance.Services.Quoting
{
    public interface IQuoteProcessorService
    {
        Task<Guid> ProcessAsync(Customer customer, Vehicle vehicle);
    }
}
