﻿using System;
using CarInsurance.Entities;

namespace CarInsurance.Services.Quoting
{
    public interface IQuoteRetrieverService
    {
        Quote Get(Guid quoteNumber);
        QuoteSummary GetSummary(Guid quoteNumber);
    }
}
