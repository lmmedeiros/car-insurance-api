﻿using System;
using CarInsurance.Entities;

namespace CarInsurance.Services.Quoting
{
    public abstract class QuoteRetrieverServiceDecoratorBase : IQuoteRetrieverService
    {
        private readonly IQuoteRetrieverService _quoteRetrieverService;

        public QuoteRetrieverServiceDecoratorBase(IQuoteRetrieverService quoteRetrieverService)
        {
            _quoteRetrieverService = quoteRetrieverService;
        }

        Quote IQuoteRetrieverService.Get(Guid quoteNumber) =>
            _quoteRetrieverService.Get(quoteNumber);

        QuoteSummary IQuoteRetrieverService.GetSummary(Guid quoteNumber) =>
            _quoteRetrieverService.GetSummary(quoteNumber);
    }
}