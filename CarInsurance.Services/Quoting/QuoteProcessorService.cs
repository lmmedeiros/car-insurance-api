﻿using System;
using System.Threading.Tasks;
using CarInsurance.Entities;

namespace CarInsurance.Services.Quoting
{
    public class QuoteProcessorService : IQuoteProcessorService
    {
        private readonly IQuoteBuilder _quoteBuilder;
        private readonly IQuoteCreationService _quoteCreationService;

        public QuoteProcessorService(IQuoteBuilder quoteBuilder, IQuoteCreationService quoteCreationService)
        {
            _quoteBuilder = quoteBuilder;
            _quoteCreationService = quoteCreationService;
        }

        async Task<Guid> IQuoteProcessorService.ProcessAsync(Customer customer, Vehicle vehicle)
        {
            var quote = _quoteBuilder.Build(customer, vehicle);

            await _quoteCreationService.CreateQuoteAsync(quote);
            
            return quote.QuoteNumber;
        }
    }
}
