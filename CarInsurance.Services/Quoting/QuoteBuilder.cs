﻿using CarInsurance.Entities;

namespace CarInsurance.Services.Quoting
{
    public class QuoteBuilder : IQuoteBuilder
    {
        Quote IQuoteBuilder.Build(Customer customer, Vehicle vehicle)
        {
            return new Quote(customer, vehicle);
        }
    }
}
