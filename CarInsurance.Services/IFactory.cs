﻿namespace CarInsurance.Services
{
    public interface IFactory<TIn, TOut>
    {
        TOut Create(TIn input);
    }
}
