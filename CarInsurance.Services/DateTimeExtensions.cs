﻿using System;

namespace CarInsurance.Services
{
    public static class DateTimeExtensions
    {
        public static int DateDifferenceInYears(this DateTime originalDate, DateTime dateToCompare)
        {
            var initialDate = new DateTime(1, 1, 1);
            var timespan = dateToCompare - originalDate;
            var years = (initialDate + timespan).Year - 1;

            return years;
        }
    }
}
