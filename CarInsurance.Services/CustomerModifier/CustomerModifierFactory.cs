﻿using System;
using CarInsurance.Entities;
using CarInsurance.Services.CustomerModifier;

namespace CarInsurance.Services.CusstomerModifier
{
    public class CustomerModifierFactory : IFactory<Customer, ICustomerModifierService>
    {
        ICustomerModifierService IFactory<Customer, ICustomerModifierService>.Create(Customer customer)
        {
            if (customer == null)
                throw new ArgumentNullException(nameof(customer));

            return customer.Gender == Gender.Female ?
                (ICustomerModifierService) new FemaleCustomerModifierService() :
                new MaleCustomerModifierService();
        }
    }
}
