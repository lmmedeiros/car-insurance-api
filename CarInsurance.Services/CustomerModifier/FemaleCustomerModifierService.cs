﻿using System;

namespace CarInsurance.Services.CustomerModifier
{
    public class FemaleCustomerModifierService : ICustomerModifierService
    {
        decimal ICustomerModifierService.CalculateModifier(int age)
        {
            if (age < 16)
                throw new ArgumentOutOfRangeException(nameof(age), "You must be at least 16 years old");

            if (age < 25) return 1.4m;
            if (age < 61) return 1m;

            return 1.2m;
        }
    }
}
