﻿using System;
using CarInsurance.Entities;

namespace CarInsurance.Services.CustomerModifier
{
    public class CustomerModifierCalculatorService : ICustomerModifierCalculatorService
    {
        private readonly IFactory<Customer, ICustomerModifierService> _customerModifierFactory;

        public CustomerModifierCalculatorService(IFactory<Customer, ICustomerModifierService> customerModifierFactory)
        {
            _customerModifierFactory = customerModifierFactory;
        }

        decimal ICustomerModifierCalculatorService.CalculateModifier(Customer customer)
        {
            if (customer == null)
                throw new ArgumentNullException(nameof(customer));

            var age = customer.BirthDate.DateDifferenceInYears(DateTime.Today);

            return _customerModifierFactory.Create(customer).CalculateModifier(age);
        }
    }
}
