﻿using CarInsurance.Entities;

namespace CarInsurance.Services.CustomerModifier
{
    public interface ICustomerModifierCalculatorService
    {
        decimal CalculateModifier(Customer customer);
    }
}