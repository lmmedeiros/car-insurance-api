﻿namespace CarInsurance.Services.CustomerModifier
{
    public interface ICustomerModifierService
    {
        decimal CalculateModifier(int age);
    }
}
