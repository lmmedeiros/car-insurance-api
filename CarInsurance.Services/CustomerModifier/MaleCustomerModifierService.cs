﻿using System;

namespace CarInsurance.Services.CustomerModifier
{
    public class MaleCustomerModifierService : ICustomerModifierService
    {
        decimal ICustomerModifierService.CalculateModifier(int age)
        {
            if (age < 16)
                throw new ArgumentOutOfRangeException(nameof(age), "You must be at least 16 years old");

            if (age < 25) return 1.5m;
            if (age < 36) return 1.2m;
            if (age < 61) return 1m;

            return 1.3m;
        }
    }
}
