﻿using System;
using CarInsurance.Services.CustomerModifier;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace CarInsurance.Services.Tests.CustomerModifier
{
    [TestClass]
    public class MaleCustomerModifierServiceTests
    {
        [TestMethod]
        [ExpectedException(typeof(ArgumentOutOfRangeException))]
        public void CalculatingModifierShouldThrowArgumentOutOfRangeExceptionWhenMaleIsLessThanSixteenYearsOld()
        {
            // Arrange
            var service = GetCustomerModifierService();

            // Act && Assert
            service.CalculateModifier(age: 15);
        }

        [TestMethod]
        public void CalculatingModifierShouldMatchModifierWhenMaleIsTwentyFourOrLess()
        {
            CheckIfModifierPerAgeMatches(age: 24, expectedModifier: 1.5m);
        }

        [TestMethod]
        public void CalculatingModifierShouldMatchModifierWhenMaleIsThirtyFiveOrLess()
        {
            CheckIfModifierPerAgeMatches(age: 35, expectedModifier: 1.2m);
        }

        [TestMethod]
        public void CalculatingModifierShouldMatchModifierWhenMaleIsSixtyOrLess()
        {
            CheckIfModifierPerAgeMatches(age: 60, expectedModifier: 1m);
        }

        [TestMethod]
        public void CalculatingModifierShouldMatchModifierWhenMaleIsOlderThanSixty()
        {
            CheckIfModifierPerAgeMatches(age: 61, expectedModifier: 1.3m);
        }

        public static ICustomerModifierService GetCustomerModifierService()
        {
            return new MaleCustomerModifierService();
        }

        private static void CheckIfModifierPerAgeMatches(int age, decimal expectedModifier)
        {
            // Arrange
            var service = GetCustomerModifierService();

            // Act
            var modifier = service.CalculateModifier(age);

            // Assert
            Assert.AreEqual(expectedModifier, modifier);
        }
    }
}
