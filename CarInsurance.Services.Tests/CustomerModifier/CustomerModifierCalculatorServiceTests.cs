﻿using System;
using CarInsurance.Entities;
using CarInsurance.Entities.Tests.MockTemplates;
using CarInsurance.Services.CustomerModifier;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace CarInsurance.Services.Tests.CustomerModifier
{
    [TestClass]
    public class CustomerModifierCalculatorServiceTests
    {
        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void CalculatingShouldThrowArgumentNullExceptionWhenCustomerIsNull()
        {
            // Arrange
            ICustomerModifierCalculatorService service = new CustomerModifierCalculatorService(
                Mock.Of<IFactory<Customer, ICustomerModifierService>>());

            // Act && Assert
            service.CalculateModifier(customer: null);
        }

        [TestMethod]
        public void CalculatingModifierShouldDelegateCalculationToFactoryInstance()
        {
            // Arrange
            const decimal expectedModifier = 1m;

            var customer = CustomerTemplate.Customer;
            var customerModifierService = Mock.Of<ICustomerModifierService>(c =>
                c.CalculateModifier(It.IsAny<int>()) == expectedModifier);

            var factory = Mock.Of<IFactory<Customer, ICustomerModifierService>>(f => 
                f.Create(customer) == customerModifierService);

            ICustomerModifierCalculatorService customerModifierCalculatorService = 
                new CustomerModifierCalculatorService(factory);

            // Act
            var modifier = customerModifierCalculatorService.CalculateModifier(customer);

            // Assert
            Assert.AreEqual(expectedModifier, modifier);
        }
    }
}
