﻿using System;
using CarInsurance.Entities;
using CarInsurance.Entities.Tests.MockTemplates;
using CarInsurance.Services.CusstomerModifier;
using CarInsurance.Services.CustomerModifier;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace CarInsurance.Services.Tests.CustomerModifier
{
    [TestClass]
    public class CustomerModifierFactoryTests
    {
        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void CreatingShouldThrowArgumentNullExceptionWhenCustomerIsNull()
        {
            // Arrange
            var factory = GetFactory();

            // Act && Assert
            var service = factory.Create(null);
        }

        [TestMethod]
        public void CreatingNewCustomerModifierServiceShouldReturnFemaleServiceWhenCustomerGenderIsFemale()
        {
            // Arrange
            var customer = new Customer("ssn", "name", Gender.Female, new DateTime(), null, "valid@email.com", null); ;
            var factory = GetFactory();

            // Act
            var service = factory.Create(customer);

            // Assert
            Assert.IsInstanceOfType(service, typeof(FemaleCustomerModifierService));
        }

        [TestMethod]
        public void CreatingNewCustomerModifierServiceShouldReturnMaleServiceWhenCustomerGenderIsMale()
        {
            // Arrange
            var customer = new Customer("ssn", "name", Gender.Male, new DateTime(), null, "valid@email.com", null); ;
            var factory = GetFactory();

            // Act
            var service = factory.Create(customer);

            // Assert
            Assert.IsInstanceOfType(service, typeof(MaleCustomerModifierService));
        }

        private static IFactory<Customer, ICustomerModifierService> GetFactory()
        {
            return new CustomerModifierFactory();
        }
    }
}
