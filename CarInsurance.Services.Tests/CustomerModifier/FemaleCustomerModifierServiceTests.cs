﻿using System;
using CarInsurance.Services.CustomerModifier;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace CarInsurance.Services.Tests.CustomerModifier
{
    [TestClass]
    public class FemaleCustomerModifierServiceTests
    {
        [TestMethod]
        [ExpectedException(typeof(ArgumentOutOfRangeException))]
        public void CalculatingModifierShouldThrowArgumentOutOfRangeExceptionWhenFemaleIsLessThanSixteenYearsOld()
        {
            // Arrange
            var service = GetCustomerModifierService();

            // Act && Assert
            service.CalculateModifier(age: 15);
        }

        [TestMethod]
        public void CalculatingModifierShouldMatchModifierWhenFemaleIsTwentyFourOrLess()
        {
            CheckIfModifierPerAgeMatches(age: 24, expectedModifier: 1.4m);
        }

        [TestMethod]
        public void CalculatingModifierShouldMatchModifierWhenFemaleIsSixtyOrLess()
        {
            CheckIfModifierPerAgeMatches(age: 60, expectedModifier: 1m);
        }

        [TestMethod]
        public void CalculatingModifierShouldMatchModifierWhenFemaleIsOlderThanSixty()
        {
            CheckIfModifierPerAgeMatches(age: 61, expectedModifier: 1.2m);
        }

        public static ICustomerModifierService GetCustomerModifierService()
        {
            return new FemaleCustomerModifierService();
        }

        private static void CheckIfModifierPerAgeMatches(int age, decimal expectedModifier)
        {
            // Arrange
            var service = GetCustomerModifierService();

            // Act
            var modifier = service.CalculateModifier(age);

            // Assert
            Assert.AreEqual(expectedModifier, modifier);
        }
    }
}
