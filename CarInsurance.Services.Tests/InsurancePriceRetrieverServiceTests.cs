﻿using System;
using CarInsurance.Entities;
using CarInsurance.Entities.Tests.MockTemplates;
using CarInsurance.Repositories;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace CarInsurance.Services.Tests
{
    [TestClass]
    public class InsurancePriceRetrieverServiceTests
    {
        private const decimal BasePrice = 1m;

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void GettingPriceShouldThrowArgumentNullExceptionWhenVehicleIsNull()
        {
            // Arrange
            var service = GetInsurancePriceRetrieverService();

            // Act && Assert
            service.GetPrice(vehicle: null);
        }

        [TestMethod]
        public void GettingPriceShouldRetrieveInsuranceMatchingCompleteVehicleInformation()
        {
            // Arrange
            var insurancePrice = GetVehicleInsurancePrice();
            var repository = Mock.Of<IVehicleInsurancePriceFinder>(v => 
                v.Find(VehicleTemplate.Vehicle) == insurancePrice);
            var service = GetInsurancePriceRetrieverService(repository);

            // Act
            var price = service.GetPrice(VehicleTemplate.Vehicle);

            // Assert
            Assert.AreEqual(BasePrice, price);
        }

        [TestMethod]
        public void GettingPriceShouldFallbackToInsuranceMatchingTypeAndYearAndMake()
        {
            // Arrange
            var insurancePrice = GetVehicleInsurancePrice(model: "different-model");
            var repository = Mock.Of<IVehicleInsurancePriceFinder>(v => 
                v.FindByTypeAndYearAndMake(insurancePrice.Type, insurancePrice.ManufacturingYear.Value, 
                    insurancePrice.Make) == insurancePrice);

            var service = GetInsurancePriceRetrieverService(repository);

            // Act
            var price = service.GetPrice(VehicleTemplate.Vehicle);

            // Assert
            Assert.AreEqual(BasePrice, price);
        }

        [TestMethod]
        public void GettingPriceShouldFallbackToInsuranceMatchingTypeAndYear()
        {
            // Arrange
            var insurancePrice = GetVehicleInsurancePrice(model: "different-model", make: "different-make");
            var repository = Mock.Of<IVehicleInsurancePriceFinder>(v =>
                v.FindByTypeAndYear(insurancePrice.Type, insurancePrice.ManufacturingYear.Value) ==  insurancePrice);

            var service = GetInsurancePriceRetrieverService(repository);

            // Act
            var price = service.GetPrice(VehicleTemplate.Vehicle);

            // Assert
            Assert.AreEqual(BasePrice, price);
        }

        [TestMethod]
        public void GettingPriceShouldFallbackToInsuranceMatchingType()
        {
            // Arrange
            var insurancePrice = GetVehicleInsurancePrice(year: 2000, model: "different-model", 
                make: "different-make");

            var repository = Mock.Of<IVehicleInsurancePriceFinder>(v =>
                v.FindByType(insurancePrice.Type) == insurancePrice);

            var service = GetInsurancePriceRetrieverService(repository);

            // Act
            var price = service.GetPrice(VehicleTemplate.Vehicle);

            // Assert
            Assert.AreEqual(BasePrice, price);
        }

        [TestMethod]
        public void GettingPriceShouldFallbackToDefaultInsurancePrice()
        {
            // Arrange
            var insurancePrice = GetVehicleInsurancePrice(VehicleType.Car, 2000, "different-model", "different-make");
            var service = GetInsurancePriceRetrieverService();

            // Act
            var price = service.GetPrice(VehicleTemplate.Vehicle);

            // Assert
            Assert.AreEqual(1000m, price);
        }

        private static VehicleInsurancePrice GetVehicleInsurancePrice(VehicleType type = VehicleType.Other, 
            int year = 2018, string model = "model", string make = "make")
        {
            return new VehicleInsurancePrice(1, BasePrice, type, year, model, make);
        }

        private static IInsurancePriceRetrieverService GetInsurancePriceRetrieverService(
            IVehicleInsurancePriceFinder vehicleInsurancePriceRepository = null)
        {
            return new InsurancePriceRetrieverService(
                vehicleInsurancePriceRepository ?? Mock.Of<IVehicleInsurancePriceFinder>());
        }
    }
}
