﻿using System;
using CarInsurance.Entities;
using CarInsurance.Entities.Tests.MockTemplates;
using CarInsurance.Repositories.Common;
using CarInsurance.Services.Quoting;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace CarInsurance.Services.Tests.Quoting
{
    [TestClass]
    public class QuoteRetrieverServiceTests
    {
        [TestMethod]
        [ExpectedException(typeof(InvalidOperationException))]
        public void GettingQuoteShouldThrowInvalidOperationExceptionWhenQuoteIsNotAvailable()
        {
            // Arrange
            IQuoteRetrieverService service = new QuoteRetrieverService(
                Mock.Of<IFinder<Guid, Quote>>(), Mock.Of<IInsurancePriceRetrieverService>());

            // Act && Assert
            service.Get(Guid.Empty);
        }

        [TestMethod]
        public void GettingQuoteShouldFindByQuoteNumber()
        {
            // Arrange
            var quote = new Quote(CustomerTemplate.Customer, VehicleTemplate.Vehicle);

            IQuoteRetrieverService service = new QuoteRetrieverService(
                Mock.Of<IFinder<Guid, Quote>>(f => f.Find(quote.QuoteNumber) == quote),
                Mock.Of<IInsurancePriceRetrieverService>());

            // Act
            var retrievedQuote = service.Get(quote.QuoteNumber);

            // Assert
            Assert.AreSame(quote, retrievedQuote);
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidOperationException))]
        public void GettingSummaryShouldThrowInvalidOperationExceptionWhenQuoteIsNotAvailable()
        {
            // Arrange
            IQuoteRetrieverService service = new QuoteRetrieverService(
                Mock.Of<IFinder<Guid, Quote>>(), Mock.Of<IInsurancePriceRetrieverService>());

            // Act && Assert
            service.GetSummary(Guid.Empty);
        }

        [TestMethod]
        public void GettingSummaryShouldRetrieveQuoteInformation()
        {
            // Arrange
            const decimal price = 1m;

            var quote = new Quote(CustomerTemplate.Customer, VehicleTemplate.Vehicle);

            IQuoteRetrieverService service = new QuoteRetrieverService(
                Mock.Of<IFinder<Guid, Quote>>(f => f.Find(quote.QuoteNumber) == quote), 
                Mock.Of<IInsurancePriceRetrieverService>(s => s.GetPrice(quote.Vehicle) == price));

            // Act
            var quoteSummary = service.GetSummary(quote.QuoteNumber);

            // Assert
            Assert.AreEqual(quote.QuoteNumber, quoteSummary.QuoteNumber);
            Assert.AreEqual(price, quoteSummary.Price);
            Assert.AreEqual(QuoteStatus.Created, quoteSummary.QuoteStatus);
        }
    }
}
