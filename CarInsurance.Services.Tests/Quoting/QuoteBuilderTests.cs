﻿using System;
using CarInsurance.Entities.Tests.MockTemplates;
using CarInsurance.Services.Quoting;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace CarInsurance.Services.Tests.Quoting
{
    [TestClass]
    public class QuoteBuilderTests
    {
        [TestMethod]
        public void BuildingQuoteShouldCreateNewQuote()
        {
            // Arrange
            IQuoteBuilder builder = new QuoteBuilder();

            // Act
            var quote = builder.Build(CustomerTemplate.Customer, VehicleTemplate.Vehicle);

            // Assert
            Assert.AreNotSame(Guid.Empty, quote.QuoteNumber);
            Assert.AreSame(CustomerTemplate.Customer, quote.Customer);
            Assert.AreSame(VehicleTemplate.Vehicle, quote.Vehicle);
        }
    }
}
