﻿using System.Threading.Tasks;
using CarInsurance.Entities;
using CarInsurance.Entities.Tests.MockTemplates;
using CarInsurance.Services.Quoting;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace CarInsurance.Services.Tests.Quoting
{
    [TestClass]
    public class QuoteProcessorTests
    {
        [TestMethod]
        public async Task ProcessingQuoteShouldBuildQuoteContainingGeneratedQuoteNumber()
        {
            // Arrange
            var quote = new Quote(CustomerTemplate.Customer, VehicleTemplate.Vehicle);
            var quoteBuilder = Mock.Of<IQuoteBuilder>(b => 
                 b.Build(quote.Customer, quote.Vehicle) == quote);

            IQuoteProcessorService quoteProcessor = new QuoteProcessorService(quoteBuilder, Mock.Of<IQuoteCreationService>());

            // Act
            var quoteNumber = await quoteProcessor.ProcessAsync(quote.Customer, quote.Vehicle);

            // Assert
            Assert.AreEqual(quote.QuoteNumber, quoteNumber);
        }

        [TestMethod]
        public async Task ProcessingQuoteShouldCreateQuoteAsynchronously()
        {
            // Arrange
            var quote = new Quote(CustomerTemplate.Customer, VehicleTemplate.Vehicle);
            var quoteBuilder = Mock.Of<IQuoteBuilder>(b =>
                 b.Build(quote.Customer, quote.Vehicle) == quote);

            var quoteCreationService = Mock.Of<IQuoteCreationService>();
            IQuoteProcessorService quoteProcessor = new QuoteProcessorService(quoteBuilder, quoteCreationService);

            // Act
            var quoteNumber = await quoteProcessor.ProcessAsync(quote.Customer, quote.Vehicle);

            // Assert
            Mock.Get(quoteCreationService).Verify(q => q.CreateQuoteAsync(quote));
        }
    }
}
