﻿using System;
using System.Threading.Tasks;
using CarInsurance.Entities;
using CarInsurance.Entities.Tests.MockTemplates;
using CarInsurance.Repositories.Common;
using CarInsurance.Services.Quoting;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace CarInsurance.Services.Tests.Quoting
{
    [TestClass]
    public class QuoteCreationServiceTests
    {
        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public async Task CreatingQuoteShouldThrowArgumentNullExceptionWhenQuoteIsNull()
        {
            // Arrange
            IQuoteCreationService quoteCreationService = new QuoteCreationService(Mock.Of<ISaveAsync<Quote>>());

            // Act && Assert
            await quoteCreationService.CreateQuoteAsync(quote: null);
        }

        [TestMethod]
        public async Task CreatingQuoteShouldSaveQuoteAsynchronously()
        {
            // Arrange
            var quote = new Quote(CustomerTemplate.Customer, VehicleTemplate.Vehicle);
            var quoteRepository = Mock.Of<ISaveAsync<Quote>>();
            IQuoteCreationService quoteCreationService = new QuoteCreationService(quoteRepository);

            // Act
            await quoteCreationService.CreateQuoteAsync(quote);

            // Assert
            Mock.Get(quoteRepository).Verify(r => r.SaveAsync(quote));
        }
    }
}
