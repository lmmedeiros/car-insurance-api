﻿using System.Collections.Generic;
using CarInsurance.Entities;
using CarInsurance.Entities.Tests.MockTemplates;
using CarInsurance.Services.Quoting;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace CarInsurance.Services.Tests.Quoting
{
    [TestClass]
    public class CachedQuoteRetrieverServiceDecoratorTests
    {
        [TestMethod]
        public void GettingQuoteShouldDelegateRetrievalToDependency()
        {
            // Arrange
            var quote = new Quote(CustomerTemplate.Customer, VehicleTemplate.Vehicle);
            var serviceDependency = Mock.Of<IQuoteRetrieverService>(q =>
                q.Get(quote.QuoteNumber) == quote);

            IQuoteRetrieverService service = new CachedQuoteRetrieverServiceDecorator(serviceDependency);

            // Act
            var retrievedQuote = service.Get(quote.QuoteNumber);

            // Assert
            Assert.AreSame(quote, retrievedQuote);
        }

        [TestMethod]
        public void GettingQuoteShouldReturnCachedQuoteWhenAvailable()
        {
            // Arrange
            var quote = new Quote(CustomerTemplate.Customer, VehicleTemplate.Vehicle);
            var cache = new List<Quote> { quote };

            IQuoteRetrieverService service = new CachedQuoteRetrieverServiceDecorator(
                Mock.Of<IQuoteRetrieverService>(), cache);

            // Act
            var retrievedQuote = service.Get(quote.QuoteNumber);

            // Assert
            Assert.AreSame(quote, retrievedQuote);
        }

    }
}
