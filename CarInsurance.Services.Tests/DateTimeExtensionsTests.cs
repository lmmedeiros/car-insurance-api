﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace CarInsurance.Services.Tests
{
    [TestClass]
    public class DateTimeExtensionsTests
    {
        [TestMethod]
        public void DateDifferenceInYearsShouldCalculateTheDifferenceInYearsBetweenTwoDates()
        {
            CheckIfDateDifferenceMatchesExpectedValue(originalDate: new DateTime(2009, 1, 2), expectedDifference: 8);
            CheckIfDateDifferenceMatchesExpectedValue(originalDate: new DateTime(2010, 1, 1), expectedDifference: 8);
            CheckIfDateDifferenceMatchesExpectedValue(originalDate: new DateTime(2010, 1, 2), expectedDifference: 7);
            CheckIfDateDifferenceMatchesExpectedValue(originalDate: new DateTime(2010, 2, 1), expectedDifference: 7);
        }

        private void CheckIfDateDifferenceMatchesExpectedValue(DateTime originalDate, int expectedDifference)
        {
            // Arrange
            var dateToCompare = new DateTime(2018, 1, 1);

            // Act
            var difference = originalDate.DateDifferenceInYears(dateToCompare);

            // Assert
            Assert.AreEqual(expectedDifference, difference);
        }
    }
}
