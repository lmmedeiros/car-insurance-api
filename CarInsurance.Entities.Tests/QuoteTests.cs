﻿using System;
using CarInsurance.Entities.Tests.MockTemplates;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace CarInsurance.Entities.Tests
{
    [TestClass]
    public class QuoteTests
    {
        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void InitializingQuoteShouldThrowArgumentNullExceptionWhenCustomerIsNull()
        {
            // Act && Assert
            new Quote(null, VehicleTemplate.Vehicle);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void InitializingQuoteShouldThrowArgumentNullExceptionWhenVehicleIsNull()
        {
            // Act && Assert
            new Quote(CustomerTemplate.Customer, null);
        }

        [TestMethod]
        public void InitializingQuoteShouldGenerateQuoteNumber()
        {
            // Arrange && Act
            var quote = new Quote(CustomerTemplate.Customer, VehicleTemplate.Vehicle);

            // Assert
            Assert.AreNotEqual(Guid.Empty, quote.QuoteNumber);
        }

        [TestMethod]
        public void InitializingQuoteShouldCreateNewInstance()
        {
            // Arrange
            var customer = CustomerTemplate.Customer;
            var vehicle = VehicleTemplate.Vehicle;

            // Act
            var quote = new Quote(customer, vehicle);

            // Assert
            Assert.AreSame(customer, quote.Customer);
            Assert.AreSame(vehicle, vehicle);
            Assert.AreEqual(QuoteStatus.Created, quote.QuoteStatus);
        }
    }
}
