﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace CarInsurance.Entities.Tests
{
    [TestClass]
    public class QuoteSummaryTests
    {
        [TestMethod]
        public void InitializingQuoteSummaryShouldCreateNewInstance()
        {
            // Arrange
            const decimal price = 1m;
            var quoteStatus = QuoteStatus.Expired;
            var quoteNumber = Guid.Parse("6ed26ad4-c7e2-437b-9c2b-f06b29391bba");

            // Act
            var quoteSummary = new QuoteSummary(quoteNumber, price, quoteStatus);

            // Assert
            Assert.AreEqual(price, quoteSummary.Price);
            Assert.AreEqual(quoteNumber, quoteSummary.QuoteNumber);
            Assert.AreEqual(quoteStatus, quoteSummary.QuoteStatus);
        }
    }
}
