﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace CarInsurance.Entities.Tests
{
    [TestClass]
    public class VehicleTests
    {
        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void InitializingVehicleShouldThrowArgumentNullExceptionWhenMakeIsNull()
        {
            new Vehicle(VehicleType.Other, 2018, "model", null);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void InitializingVehicleShouldThrowArgumentNullExceptionWhenModelIsNull()
        {
            new Vehicle(VehicleType.Other, 2018, null, "make");
        }

        [TestMethod]
        public void InitializingVehicleShouldCreateNewInstance()
        {
            // Arrange
            const VehicleType type = VehicleType.Other;
            const string model = "model";
            const string make = "make";
            const int manufacturingYear = 2018;

            // Act
            var vehicle = new Vehicle(type, manufacturingYear, model, make);

            // Assert
            Assert.AreEqual(type, vehicle.Type);
            Assert.AreEqual(manufacturingYear, vehicle.ManufacturingYear);
            Assert.AreEqual(model, vehicle.Model);
            Assert.AreEqual(make, vehicle.Make);
        }
    }
}
