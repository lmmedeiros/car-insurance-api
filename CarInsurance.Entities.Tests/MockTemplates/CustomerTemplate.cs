﻿using System;

namespace CarInsurance.Entities.Tests.MockTemplates
{
    public sealed class CustomerTemplate
    {
        private static Customer _customer;

        public static Customer Customer
        {
            get
            {
                if (_customer == null)
                    _customer = new Customer("ssn", "name", Gender.Male, new DateTime(), null, "valid@email.com", null);

                return _customer;
            }
        }
    }
}
