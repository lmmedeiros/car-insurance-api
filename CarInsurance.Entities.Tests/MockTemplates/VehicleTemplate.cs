﻿namespace CarInsurance.Entities.Tests.MockTemplates
{
    public sealed class VehicleTemplate
    {
        private static Vehicle _vehicle;

        public static Vehicle Vehicle
        {
            get
            {
                if (_vehicle == null)
                    _vehicle = new Vehicle(VehicleType.Other, 2018, "model", "make");

                return _vehicle;
            }
        }
    }
}
