﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace CarInsurance.Entities.Tests
{
    [TestClass]
    public class CustomerTests
    {
        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void InitializingCustomerShouldThrowArgumentNullExceptionWhenSocialSecurityNumberIsNull()
        {
            // Act && Assert
            new Customer(null, "name", Gender.Male, new DateTime(), "adress", "email", "phone");
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void InitializingCustomerShouldThrowArgumentNullExceptionWhenNameIsNull()
        {
            // Act && Assert
            new Customer("ssn", null, Gender.Male, new DateTime(), "adress", "email", "phone");
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void InitializingCustomerShouldThrowArgumentNullExceptionWhenEmailIsNull()
        {
            // Act && Assert
            new Customer("ssn", "name", Gender.Male, new DateTime(), "adress", null, "phone");
        }

        [TestMethod]
        [ExpectedException(typeof(FormatException))]
        public void InitializingCustomerShouldThrowFormatExceptionWhenEmailIsInvalid()
        {
            // Act && Assert
            new Customer("ssn", "name", Gender.Male, new DateTime(), "adress", "email", "phone");
        }

        [TestMethod]
        public void InitializingCustomerShouldCreateNewInstance()
        {
            // Arrange
            const string ssn = "ssn";
            const string name = "name";
            const string email = "valid@email.com";
            const Gender gender = Gender.Male;
            var birthDate = new DateTime();

            // Act
            var customer = new Customer(ssn, name, gender, birthDate, null, email, null);

            // Assert
            Assert.AreEqual(ssn, customer.SocialSecurityNumber);
            Assert.AreEqual(name, customer.Name);
            Assert.AreEqual(email, customer.Email);
            Assert.AreEqual(gender, customer.Gender);
            Assert.AreEqual(birthDate, customer.BirthDate);
            Assert.IsNull(customer.Address);
            Assert.IsNull(customer.PhoneNumber);
        }
    }
}
