﻿using CarInsurance.Entities.Tests.MockTemplates;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace CarInsurance.Entities.Tests
{
    [TestClass]
    public class VehicleInsurancePriceTests
    {
        [TestMethod]
        public void InitializingVehicleInsurancePriceShouldCreateNewInstance()
        {
            // Arrange && Act
            const int incidentAveragePerYear = 1;
            const decimal basePrice = 1m;

            var vehicleInsurancePrice = new VehicleInsurancePrice(incidentAveragePerYear, basePrice, 
                VehicleTemplate.Vehicle.Type, VehicleTemplate.Vehicle.ManufacturingYear, VehicleTemplate.Vehicle.Model,
                VehicleTemplate.Vehicle.Make);

            // Assert
            Assert.AreEqual(VehicleTemplate.Vehicle.Make, vehicleInsurancePrice.Make);
            Assert.AreEqual(VehicleTemplate.Vehicle.Model, vehicleInsurancePrice.Model);
            Assert.AreEqual(VehicleTemplate.Vehicle.ManufacturingYear, vehicleInsurancePrice.ManufacturingYear);
            Assert.AreEqual(VehicleTemplate.Vehicle.Type, vehicleInsurancePrice.Type);
            Assert.AreEqual(incidentAveragePerYear, vehicleInsurancePrice.IncidentAveragePerYear);
            Assert.AreEqual(basePrice, vehicleInsurancePrice.BasePrice);
        }
    }
}
