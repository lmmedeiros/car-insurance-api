﻿using System.Linq;
using CarInsurance.Repositories.DataContext;

namespace CarInsurance.Repositories
{
    public class CustomerRepository : ICustomerRepository
    {
        private readonly CarInsuranceContext _context;

        public CustomerRepository(CarInsuranceContext context)
        {
            _context = context;
        }

        bool ICustomerRepository.Exists(string socialSecurityNumber)
        {
            return _context.Customers.Any(c => c.SocialSecurityNumber == socialSecurityNumber);
        }
    }
}
