﻿using CarInsurance.Entities;
using CarInsurance.Repositories.Common;

namespace CarInsurance.Repositories
{
    public interface IVehicleInsurancePriceFinder : IFinder<Vehicle, VehicleInsurancePrice>
    {
        VehicleInsurancePrice FindByType(VehicleType type);
        VehicleInsurancePrice FindByTypeAndYear(VehicleType type, int year);
        VehicleInsurancePrice FindByTypeAndYearAndMake(VehicleType type, int year, string make);
    }
}
