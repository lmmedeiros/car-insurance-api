﻿using System.Threading.Tasks;
using CarInsurance.Entities;
using CarInsurance.Repositories.Common;
using CarInsurance.Repositories.DataContext;

namespace CarInsurance.Repositories
{
    public sealed class QuoteRepository : ISaveAsync<Quote>
    {
        private readonly CarInsuranceContext _context;
        private readonly ICustomerRepository _customerRepository;

        public QuoteRepository(CarInsuranceContext context, ICustomerRepository customerRepository)
        {
            _context = context;
            _customerRepository = customerRepository;
        }

        async Task ISaveAsync<Quote>.SaveAsync(Quote quote)
        {
            if (_customerRepository.Exists(quote.Customer.SocialSecurityNumber))
                _context.Customers.Attach(quote.Customer);

            quote.QuoteStatus = QuoteStatus.Saved;
            _context.Quotes.Add(quote);

            await _context.SaveChangesAsync();
        }
    }
}
