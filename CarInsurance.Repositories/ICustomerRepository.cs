﻿namespace CarInsurance.Repositories
{
    public interface ICustomerRepository
    {
        bool Exists(string socialSecurityNumber);
    }
}
