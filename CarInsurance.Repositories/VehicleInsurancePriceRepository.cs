﻿using System.Linq;
using CarInsurance.Entities;
using CarInsurance.Repositories.Common;
using CarInsurance.Repositories.DataContext;

namespace CarInsurance.Repositories
{
    public class VehicleInsurancePriceRepository : IVehicleInsurancePriceFinder
    {
        private readonly CarInsuranceContext _context;

        public VehicleInsurancePriceRepository(CarInsuranceContext context)
        {
            _context = context;
        }

        VehicleInsurancePrice IFinder<Vehicle, VehicleInsurancePrice>.Find(Vehicle vehicle)
        {
            return _context.VehicleInsurancePrices.FirstOrDefault(p =>
                    p.Type == vehicle.Type &&
                    p.ManufacturingYear == vehicle.ManufacturingYear &&
                    p.Make == vehicle.Make &&
                    p.Model == vehicle.Model);
        }

        VehicleInsurancePrice IVehicleInsurancePriceFinder.FindByType(VehicleType type)
        {
            return _context.VehicleInsurancePrices.FirstOrDefault(p => p.Type == type);
        }

        VehicleInsurancePrice IVehicleInsurancePriceFinder.FindByTypeAndYear(VehicleType type, int year)
        {
            return _context.VehicleInsurancePrices.FirstOrDefault(p => 
                    p.Type == type &&
                    p.ManufacturingYear == year);
        }

        VehicleInsurancePrice IVehicleInsurancePriceFinder.FindByTypeAndYearAndMake(
            VehicleType type, int year, string make)
        {
            return _context.VehicleInsurancePrices.FirstOrDefault(p =>
                    p.Type == type &&
                    p.ManufacturingYear == year &&
                    p.Make == make);
        }
    }
}
