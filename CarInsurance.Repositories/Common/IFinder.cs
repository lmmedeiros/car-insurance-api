﻿namespace CarInsurance.Repositories.Common
{
    public interface IFinder<TIn, TOut>
    {
        TOut Find(TIn input);
    }
}
