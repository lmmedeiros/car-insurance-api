﻿using System.Threading.Tasks;

namespace CarInsurance.Repositories.Common
{
    public interface ISaveAsync<T>
    {
        Task SaveAsync(T model);
    }
}
