﻿using System.Data.Entity;
using CarInsurance.Entities;

namespace CarInsurance.Repositories.DataContext
{
    public class CarInsuranceContext : DbContext
    {
        public CarInsuranceContext() : base("CarInsurance")
        {
            Database.SetInitializer(new DropCreateDatabaseIfModelChanges<CarInsuranceContext>());

            Configuration.LazyLoadingEnabled = false;
            Configuration.ProxyCreationEnabled = false;
        }

        public virtual DbSet<Customer> Customers { get; set; }

        public virtual DbSet<Quote> Quotes { get; set; }

        public virtual DbSet<VehicleInsurancePrice> VehicleInsurancePrices { get; set; }

        public virtual DbSet<Vehicle> Vehicles { get; set; }
    }
}
