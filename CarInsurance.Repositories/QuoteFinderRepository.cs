﻿using System;
using System.Data.Entity;
using System.Linq;
using CarInsurance.Entities;
using CarInsurance.Repositories.Common;
using CarInsurance.Repositories.DataContext;

namespace CarInsurance.Repositories
{
    public class QuoteFinderRepository : IFinder<Guid, Quote>
    {
        private readonly CarInsuranceContext _context;

        public QuoteFinderRepository(CarInsuranceContext context)
        {
            _context = context;
        }

        Quote IFinder<Guid, Quote>.Find(Guid quoteNumber)
        {
            return _context.Quotes
                    .Include(q => q.Customer)
                    .Include(q => q.Vehicle)
                    .FirstOrDefault(q => q.QuoteNumber == quoteNumber);                    
        }
    }
}
