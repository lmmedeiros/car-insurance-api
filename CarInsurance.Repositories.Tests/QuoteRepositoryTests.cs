﻿using CarInsurance.Entities;
using CarInsurance.Entities.Tests.MockTemplates;
using CarInsurance.Repositories.Common;
using CarInsurance.Repositories.DataContext;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace CarInsurance.Repositories.Tests
{
    [TestClass]
    public class QuoteRepositoryTests
    {
        [TestMethod]
        public void SavingShouldNotUpdateNewCustomers()
        {
            // Arrange
            var context = GetContext();
            var quoteRepository = GetQuoteRepository(context, existentCustomer: false);
            var quote = GetQuote();

            // Act
            quoteRepository.SaveAsync(quote);

            // Assert
            Mock.Get(context.Customers).Verify(c => c.Attach(quote.Customer), Times.Never);
        }

        [TestMethod]
        public void SavingShouldUpdateCustomerWhenCustomerAlreadyExists()
        {
            // Arrange
            var context = Mock.Of<CarInsuranceContext>(c =>
                c.Customers == MockHelper.GetQueryable(CustomerTemplate.Customer));

            var quoteRepository = GetQuoteRepository(context, existentCustomer: true);
            var quote = GetQuote();

            // Act
            quoteRepository.SaveAsync(quote);

            // Assert
            Mock.Get(context.Customers).Verify(c => c.Attach(quote.Customer));
        }

        [TestMethod]
        public void SavingShouldAddNewQuote()
        {
            // Arrange
            var context = GetContext();
            var quoteRepository = GetQuoteRepository(context);
            var quote = GetQuote();

            // Act
            quoteRepository.SaveAsync(quote);

            // Assert
            Mock.Get(context.Quotes).Verify(q => q.Add(quote));
        }

        [TestMethod]
        public void SavingShouldModifyQuoteStatus()
        {
            // Arrange
            var context = Mock.Of<CarInsuranceContext>();
            context.Quotes = MockHelper.GetQueryable<Quote>();

            var quoteRepository = GetQuoteRepository(context);
            var quote = GetQuote();

            // Act
            quoteRepository.SaveAsync(quote);

            // Assert
            Assert.AreEqual(QuoteStatus.Saved, quote.QuoteStatus);
        }

        [TestMethod]
        public void SavingShouldSaveAsynchronously()
        {
            // Arrange
            var context = Mock.Of<CarInsuranceContext>();
            context.Quotes = MockHelper.GetQueryable<Quote>();

            var quoteRepository = GetQuoteRepository(context);
            var quote = GetQuote();

            // Act
            quoteRepository.SaveAsync(quote);

            // Assert
            Mock.Get(context).Verify(c => c.SaveChangesAsync());
        }

        private static CarInsuranceContext GetContext()
        {
            return Mock.Of<CarInsuranceContext>(c =>
                c.Customers == MockHelper.GetQueryable<Customer>() &&
                c.Quotes == MockHelper.GetQueryable<Quote>());
        }

        private static Quote GetQuote() =>
            new Quote(CustomerTemplate.Customer, VehicleTemplate.Vehicle);

        private static ISaveAsync<Quote> GetQuoteRepository(CarInsuranceContext context,
            bool existentCustomer = false)
        {
            var customerRepository = Mock.Of<ICustomerRepository>(c => 
                c.Exists(CustomerTemplate.Customer.SocialSecurityNumber) == existentCustomer);

            return new QuoteRepository(context, customerRepository);
        }
    }
}
