﻿using CarInsurance.Entities.Tests.MockTemplates;
using CarInsurance.Repositories.DataContext;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace CarInsurance.Repositories.Tests
{
    [TestClass]
    public class CustomerRepositoryTests
    {
        [TestMethod]
        public void CheckingIfExistsShouldBeTrueIfContextContainsCustomerWithDefinedSocialSecurityNumber()
        {
            // Arrange
            var context = GetContext();
            ICustomerRepository repository = new CustomerRepository(context);

            // Act
            var exists = repository.Exists("ssn");

            // Assert
            Assert.IsTrue(exists);
        }

        [TestMethod]
        public void CheckingIfExistsShouldBeFalseIfContextDoesNotContainCustomerWithDefinedSocialSecurityNumber()
        {
            // Arrange
            var context = GetContext();
            ICustomerRepository repository = new CustomerRepository(context);

            // Act
            var exists = repository.Exists("invalid ssn");

            // Assert
            Assert.IsFalse(exists);
        }

        private static CarInsuranceContext GetContext()
        {
            return Mock.Of<CarInsuranceContext>(c =>
                c.Customers == MockHelper.GetQueryable(CustomerTemplate.Customer));
        }
    }
}
