﻿using System;
using CarInsurance.Entities;
using CarInsurance.Entities.Tests.MockTemplates;
using CarInsurance.Repositories.Common;
using CarInsurance.Repositories.DataContext;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace CarInsurance.Repositories.Tests
{
    [TestClass]
    public class QuoteFinderRepositoryTests
    {
        [TestMethod]
        public void FindingQuoteShouldFindQuoteWithMatchingQuoteNumber()
        {
            // Arrange
            var quote = new Quote(CustomerTemplate.Customer, VehicleTemplate.Vehicle);

            var context = Mock.Of<CarInsuranceContext>(c =>
                c.Quotes == MockHelper.GetQueryable(quote));

            IFinder<Guid, Quote> repository = new QuoteFinderRepository(context);

            // Act
            var retrievedQuote = repository.Find(quote.QuoteNumber);

            // Assert
            Assert.AreSame(quote, retrievedQuote);
        }

        [TestMethod]
        public void FindingQuoteShouldReturnNullThereIsNoMatchingQuote()
        {
            // Arrange
            var quoteNumber = Guid.Empty;
            IFinder<Guid, Quote> repository = new QuoteFinderRepository(
                Mock.Of<CarInsuranceContext>(c => c.Quotes == MockHelper.GetQueryable<Quote>()));

            // Act
            var retrievedQuote = repository.Find(quoteNumber);

            // Assert
            Assert.IsNull(retrievedQuote);
        }
    }
}
