﻿using CarInsurance.Entities;
using CarInsurance.Repositories.Common;
using CarInsurance.Repositories.DataContext;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace CarInsurance.Repositories.Tests
{
    [TestClass]
    public class VehicleInsurancePriceRepositoryTests
    {
        private const decimal BasePrice = 1m;

        [TestMethod]
        public void FindingInsurancePriceShouldReturnFirstInsuranceMatchingCompleteVehicleInformation()
        {
            // Arrange
            var vehicle = GetVehicle();
            var repository = GetVehicleInsurancePriceRepository();

            // Act
            var insurancePrice = repository.Find(vehicle);

            // Assert
            Assert.AreEqual(BasePrice, insurancePrice.BasePrice);
        }

        [TestMethod]
        public void FindingInsurancePriceShouldReturnNullWhenThereIsNoInsuranceMatchingCompleteVehicleInformation()
        {
            // Arrange
            var vehicle = GetVehicle(year: 2000);
            var repository = GetVehicleInsurancePriceRepository();

            // Act
            var insurancePrice = repository.Find(vehicle);

            // Assert
            Assert.IsNull(insurancePrice);
        }

        [TestMethod]
        public void FindingInsurancePriceByTypeShouldReturnFirstMatchingInsurance()
        {
            // Arrange
            var vehicle = GetVehicle(year: 2000, model: "different-model", make: "different-make");
            var repository = GetVehicleInsurancePriceRepository();

            // Act
            var insurancePrice = repository.FindByType(vehicle.Type);

            // Assert
            Assert.AreEqual(BasePrice, insurancePrice.BasePrice);
        }

        [TestMethod]
        public void FindingInsurancePriceByTypeShouldReturnNullWhenThereIsNoInsuranceMatchingType()
        {
            // Arrange
            var vehicle = GetVehicle(type: VehicleType.Car);
            var repository = GetVehicleInsurancePriceRepository();

            // Act
            var insurancePrice = repository.FindByType(vehicle.Type);

            // Assert
            Assert.IsNull(insurancePrice);
        }

        [TestMethod]
        public void FindingInsurancePriceByTypeAndYearShouldReturnFirstMatchingInsurance()
        {
            // Arrange
            var vehicle = GetVehicle(model: "different-model", make: "different-make");
            var repository = GetVehicleInsurancePriceRepository();

            // Act
            var insurancePrice = repository.FindByTypeAndYear(vehicle.Type, vehicle.ManufacturingYear);

            // Assert
            Assert.AreEqual(BasePrice, insurancePrice.BasePrice);
        }

        [TestMethod]
        public void FindingInsurancePriceByTypeAndYearShouldReturnNullWhenThereIsNoInsuranceMatchingType()
        {
            // Arrange
            var vehicle = GetVehicle(type: VehicleType.Car);
            var repository = GetVehicleInsurancePriceRepository();

            // Act
            var insurancePrice = repository.FindByTypeAndYear(vehicle.Type, vehicle.ManufacturingYear);

            // Assert
            Assert.IsNull(insurancePrice);
        }

        [TestMethod]
        public void FindingInsurancePriceByTypeAndYearShouldReturnNullWhenThereIsNoInsuranceMatchingYear()
        {
            // Arrange
            var vehicle = GetVehicle(year: 2000);
            var repository = GetVehicleInsurancePriceRepository();

            // Act
            var insurancePrice = repository.FindByTypeAndYear(vehicle.Type, vehicle.ManufacturingYear);

            // Assert
            Assert.IsNull(insurancePrice);
        }

        [TestMethod]
        public void FindingInsurancePriceByTypeAndYearAndMakeShouldReturnFirstMatchingInsurance()
        {
            // Arrange
            var vehicle = GetVehicle(model: "different-model");
            var repository = GetVehicleInsurancePriceRepository();

            // Act
            var insurancePrice = repository.FindByTypeAndYearAndMake(
                vehicle.Type, vehicle.ManufacturingYear, vehicle.Make);

            // Assert
            Assert.AreEqual(BasePrice, insurancePrice.BasePrice);
        }

        [TestMethod]
        public void FindingInsurancePriceByTypeAndYearAndMakeShouldReturnNullWhenThereIsNoInsuranceMatchingMake()
        {
            // Arrange
            var vehicle = GetVehicle(make: "different-make");
            var repository = GetVehicleInsurancePriceRepository();

            // Act
            var insurancePrice = repository.FindByTypeAndYearAndMake(vehicle.Type, vehicle.ManufacturingYear, vehicle.Make);

            // Assert
            Assert.IsNull(insurancePrice);
        }

        [TestMethod]
        public void FindingInsurancePriceByTypeAndYearAndMakeShouldReturnNullWhenThereIsNoInsuranceMatchingType()
        {
            // Arrange
            var vehicle = GetVehicle(type: VehicleType.Car);
            var repository = GetVehicleInsurancePriceRepository();

            // Act
            var insurancePrice = repository.FindByTypeAndYearAndMake(vehicle.Type, vehicle.ManufacturingYear, vehicle.Make);

            // Assert
            Assert.IsNull(insurancePrice);
        }

        [TestMethod]
        public void FindingInsurancePriceByTypeAndYearAndMakeShouldReturnNullWhenThereIsNoInsuranceMatchingYear()
        {
            // Arrange
            var vehicle = GetVehicle(year: 2000);
            var repository = GetVehicleInsurancePriceRepository();

            // Act
            var insurancePrice = repository.FindByTypeAndYearAndMake(vehicle.Type, vehicle.ManufacturingYear, vehicle.Make);

            // Assert
            Assert.IsNull(insurancePrice);
        }
        
        private static Vehicle GetVehicle(VehicleType type = VehicleType.Other, int year = 2018, 
            string model = "model", string make = "make")
        {
            return new Vehicle(type, year, model, make);
        }

        private static IVehicleInsurancePriceFinder GetVehicleInsurancePriceRepository()
        {
            var vehicle = GetVehicle();
            var vehicleInsurancePrice = new VehicleInsurancePrice(1, BasePrice, vehicle.Type, 
                vehicle.ManufacturingYear, vehicle.Model, vehicle.Make);

            var context = Mock.Of<CarInsuranceContext>(c =>
                c.VehicleInsurancePrices == MockHelper.GetQueryable(vehicleInsurancePrice));

            return new VehicleInsurancePriceRepository(context);
        }
    }
}
