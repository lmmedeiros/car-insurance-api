﻿using System.Data.Entity;
using System.Linq;
using Moq;

namespace CarInsurance.Repositories.Tests
{
    public static class MockHelper
    {
        /// <summary>
        ///    Helper to provide the actual implementation to the DbSet enabling to test the DbContext dependencies
        ///    Method available <a href="http://www.loganfranken.com/blog/517/mocking-dbset-queries-in-ef6/">here</a>.
        /// </summary>
        /// <typeparam name="T">Entities that have a DbSet configured in the context</typeparam>
        /// <param name="sourceList">Set of elements that will compose the DbSet contenxt</param>
        /// <returns>Queryable DbSet</returns>
        public static DbSet<T> GetQueryable<T>(params T[] sourceList) where T : class
        {
            var queryable = sourceList.AsQueryable();

            var dbSet = new Mock<DbSet<T>>();
            dbSet.As<IQueryable<T>>().Setup(m => m.Provider).Returns(queryable.Provider);
            dbSet.As<IQueryable<T>>().Setup(m => m.Expression).Returns(queryable.Expression);
            dbSet.As<IQueryable<T>>().Setup(m => m.ElementType).Returns(queryable.ElementType);
            dbSet.As<IQueryable<T>>().Setup(m => m.GetEnumerator()).Returns(() => queryable.GetEnumerator());
            dbSet.Setup(s => s.Include(It.IsAny<string>())).Returns(dbSet.Object);

            return dbSet.Object;
        }
    }
}
